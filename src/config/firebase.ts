// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { GoogleAuthProvider, getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
import { getFunctions } from "firebase/functions";
import { getMessaging, getToken, onMessage } from "firebase/messaging";
import { getStorage } from "firebase/storage";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyByi9wLufe17zB5vUz9_4o-bleIrOHJJNc",
  authDomain: "chat-web-app-937ad.firebaseapp.com",
  projectId: "chat-web-app-937ad",
  storageBucket: "chat-web-app-937ad.appspot.com",
  messagingSenderId: "1070368817164",
  appId: "1:1070368817164:web:d6add8c950b21e84372e29",
  measurementId: "G-7XE91PRPVK",
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const storage = getStorage();
export const db = getFirestore();

export const googleProvider = new GoogleAuthProvider();

export const messaging = getMessaging(app);
export const functions = getFunctions(app);

export const requestForToken = async () => {
  return getToken(messaging, { vapidKey: "BFJQOM9dShJgUFkFU1W5yYakMiDMLOPr6psIZSsM6BZZXaH8bEHP85uuOmJbYyazuClEZOCf1WkbrlDczYnhcQw" })       //vapid-key // Cloud messing > Web configuration
    .then((currentToken) => {
      if (currentToken) {
        console.log('current token for client: ', currentToken);
        // Perform any other neccessary action with the token
        return currentToken
      } else {
        // Show permission request UI
        console.log('No registration token available. Request permission to generate one.');
        return null
      }
    })
    .catch((err) => {
      console.log('An error occurred while retrieving token. ', err);
      return null
    });
};

export const onMessageListener = async () => {
  new Promise((resolve) => {
    onMessage(messaging, (payload) => {
      console.log("payload", payload)
      resolve(payload);
    });
  });
}