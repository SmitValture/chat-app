import { useEffect } from 'react'
import toast, { Toaster } from 'react-hot-toast';

const Notification = ({ notification }: any) => {
  // const [notification, setNotification] = useState({title: '', body: ''});
  const notify = () =>  toast(<ToastDisplay/>);
  function ToastDisplay() {
    return (
      <div>
        <p><b>{notification?.title}</b></p>
        <p>{notification?.body}</p>
      </div>
    );
  };

  useEffect(() => {
    console.log("🚀 ~ Notification ~ notification:", notification)
    if (notification?.title ){
     notify()
    }
  }, [notification])

  return (
     <Toaster />
  )
}

export default Notification