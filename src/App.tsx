import { ReactNode, useContext, useEffect } from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import { Home } from "./pages/Home";
import { Login } from "./pages/Login";
import { Register } from "./pages/Register";
import { AuthContext } from "./context/AuthContext";
import { db, functions, messaging, requestForToken } from "./config/firebase";
import { onMessage } from "firebase/messaging";
import toast, { Toaster } from "react-hot-toast";
import { CombinedChatContext } from "./context/ChatContext";
import { doc, setDoc, updateDoc } from "firebase/firestore";

interface Notication {
  title?: string;
  body?: string;
}

function App() {
  const currentUser = useContext(AuthContext);
  const { data, dispatch } = useContext(CombinedChatContext);
  // console.log(currentUser);

  const ProtectedRoute = ({ children }: { children: ReactNode }) => {
    if (!currentUser) {
      return <Navigate to={"/login"} />;
    }
    return children;
  };

  useEffect(() => {
    requestForToken().then((token) => {
      dispatch({ type: "SET_DEVICE_TOKEN", payload: token });
    })
  }, []);

  useEffect(() => {
    // Ensure currentUser is defined before accessing its properties
    if (currentUser) {
      const uid = currentUser.uid;
      const uuid = localStorage.getItem("user");

      if (uuid) {
        // Correctly using setDoc with merge: true for updating the document
        setDoc(doc(db, "users", uuid), {
          uid: uid,
          displayName: currentUser.displayName,
          email: currentUser.email,
          photoURL: currentUser.photoURL,
          deviceToken: data.deviceToken,
        }, { merge: true }).then((res) => {
          console.log("Document updated successfully:", res);
        });
      }
    }
  }, [data.deviceToken]);

  useEffect(() => {
    const unsubscribe = onMessage(messaging, ({ notification, data }) => {
      console.log("payload?.notification", notification, data);
      notify({ title: notification?.title, body: notification?.body });
    });

    return () => unsubscribe(); // Cleanup function to remove the listener
  }, []);

  const notify = ({ title, body }: Notication) => toast(<ToastDisplay title={title} body={body} />);
  function ToastDisplay({ title, body }: Notication) {
    console.log("🚀 ~ ToastDisplay ~ title:", title)
    return (
      <div>
        <p><b>{title}</b></p>
        <p>{body}</p>
      </div>
    );
  }

  return (
    <>
      <Routes>
        <Route path="/register" element={<Register />} />
        <Route path="/login" element={<Login />} />
        <Route
          path="/"
          element={
            <ProtectedRoute>
              <Home />
              {/* <NotificationComponent notification={notification} /> */}
              <Toaster />
            </ProtectedRoute>
          }
        />
      </Routes>
    </>
  );
}

export default App;
